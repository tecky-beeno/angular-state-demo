import {Component} from '@angular/core'
import {Observable, timer} from 'rxjs'
import {map} from 'rxjs/operators'
import {ImageService} from './image.service'
import {FormatService} from "./format.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'angular-clock-demo'
  delay = 0
  interval = 1000
  clock: Observable<string> = timer(this.delay, this.interval).pipe(
    map(() => this.calcClock()),
  )
  keyword = 'loading'
  image = this.getImage()
  unit = this.formatService.getUnit()

  constructor(private imageService: ImageService,
              private formatService: FormatService) {
  }

  getImage(): Observable<string> {
    return this.imageService.getRandomImageUrl(this.keyword)
  }

  calcClock() {
    return new Date().toLocaleString()
  }

  setUnit(event: Event) {
    let value = (event.target as HTMLInputElement).valueAsNumber
    this.formatService.setUnit(value)
  }
}
