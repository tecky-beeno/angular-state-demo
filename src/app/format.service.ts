import {Injectable} from '@angular/core';
import {Subject} from "rxjs";
import {startWith} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class FormatService {
  private _unit = 10
  private unitSubject = new Subject<number>()

  setUnit(value: number) {
    if (value < 2) {
      return
    }
    if (value > 36) {
      return;
    }
    this._unit = value
    this.unitSubject.next(value)
  }

  getUnit() {
    return this.unitSubject.asObservable()
      .pipe(startWith(this._unit))
  }

  constructor() {
  }
}
