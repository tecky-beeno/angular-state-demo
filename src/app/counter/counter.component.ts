import { Component, OnInit } from '@angular/core';
import {CounterService} from "../counter.service";

@Component({
  selector: 'app-counter',
  templateUrl: './counter.component.html',
  styleUrls: ['./counter.component.scss']
})
export class CounterComponent implements OnInit {
counter=0
  constructor(public counterService:CounterService) { }

  ngOnInit(): void {
  }

  inc(){
  this.counter ++
  }
}
