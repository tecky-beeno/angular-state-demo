import { HttpClient } from '@angular/common/http'
import { Injectable } from '@angular/core'
import { Observable } from 'rxjs'
import { map } from 'rxjs/operators'

@Injectable({
  providedIn: 'root',
})
export class ImageService {
  constructor(private http: HttpClient) {}


  public getRandomImageUrl(keyword:string): Observable<string> {
    const apiKey = 'e55094ebb34c4e37b263d4e734a9ca11'
    const url = `https://api.giphy.com/v1/gifs/random?api_key=${apiKey}&tag=${keyword}&rating=G`
    return this.http.get(url).pipe(map((x: any) => x.data.image_url))
  }
}
