import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {HttpClientModule} from '@angular/common/http'

import { AppComponent } from './app.component';
import { ImageService } from './image.service';
import {FormsModule} from "@angular/forms";
import { CounterComponent } from './counter/counter.component';
import { FormatPipe } from './format.pipe';

@NgModule({
  declarations: [
    AppComponent,
    CounterComponent,
    FormatPipe,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,

  ],
  providers: [ImageService],
  bootstrap: [AppComponent]
})
export class AppModule { }
