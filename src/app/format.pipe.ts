import {Pipe, PipeTransform} from '@angular/core';
import {FormatService} from "./format.service";
import {Observable} from "rxjs";
import {map} from "rxjs/operators";

@Pipe({
  name: 'format'
})
export class FormatPipe implements PipeTransform {
  constructor(private formatService: FormatService) {
  }

  transform(value: number, ...args: unknown[]): Observable<string> {
    return this.formatService.getUnit()
      .pipe(map(unit => value.toString(unit)))
  }

}
